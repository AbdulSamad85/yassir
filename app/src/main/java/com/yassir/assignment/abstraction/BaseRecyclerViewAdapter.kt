package com.yassir.assignment.abstraction

import android.content.Context
import android.view.LayoutInflater
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerViewAdapter protected constructor(
    protected val context: Context,
    data: List<*>?
) :
    RecyclerView.Adapter<BaseViewHolder?>() {

    protected val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    var dataList: List<*>?

    init {
        dataList = data
    }

    protected fun getStringRes(@StringRes stringRes: Int): String {
        return context.getString(stringRes)
    }

    protected fun getColorRes(@ColorRes colorRes: Int): Int {
        return ContextCompat.getColor(context, colorRes)
    }

    protected fun getData(index: Int): Any {
        return dataList!![index]!!
    }

    fun setData(data: List<*>?) {
        dataList = data
    }

    override fun getItemCount(): Int {
        return if (dataList != null) dataList!!.size else 0
    }
}
