package com.yassir.assignment.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.yassir.assignment.ui.movieDetails.MovieDetailFragment
import com.yassir.assignment.ui.movieList.MoviesFragment
import javax.inject.Inject

class MoviesFragmentFactory
@Inject
constructor() : FragmentFactory() {
    override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
        return when (className) {
            MoviesFragment::class.java.name -> {
                MoviesFragment()
            }
            MovieDetailFragment::class.java.name -> {
                MovieDetailFragment()
            }
            else -> return super.instantiate(classLoader, className)
        }
    }
}
