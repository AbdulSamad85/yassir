package com.yassir.assignment

import com.yassir.assignment.network.MovieService
import com.yassir.assignment.repository.MovieRepository
import com.yassir.assignment.repository.MovieRepositoryImpl
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@RunWith(JUnit4::class)
class MovieRepositoryTest : BaseTest() {

    lateinit var movieRepository: MovieRepository

    @Mock
    lateinit var movieService: MovieService

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        movieRepository = MovieRepositoryImpl(movieService)
    }

    @Test
    fun `get movies test`() {
        val anyPage = 1

        val expectedResponse = getMoviesResponse()
        runBlocking {
            Mockito.`when`(movieRepository.getMovies(anyPage))
                .thenReturn(
                    Response.success(
                        expectedResponse
                    )
                )
            val response = movieRepository.getMovies(anyPage)
            assertEquals(expectedResponse, response.body())
        }
    }

    @Test
    fun `get movie detail test`() {
        val expectedResponse = getMovieDetailResponse()
        runBlocking {
            Mockito.`when`(movieRepository.getMovieDetails(movieId))
                .thenReturn(
                    Response.success(expectedResponse)
                )
            val response = movieRepository.getMovieDetails(movieId)
            assertEquals(expectedResponse, response.body())
        }
    }
}
