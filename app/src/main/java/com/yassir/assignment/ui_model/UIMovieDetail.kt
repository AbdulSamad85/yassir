package com.yassir.assignment.ui_model

import android.os.Parcelable
import com.yassir.assignment.network.io.model.movie.Movie
import kotlinx.parcelize.Parcelize

/**
 * UIModel for custom Banner view
 */
@Parcelize
data class UIMovieDetail(
    val title: String,
    val year: String,
    val imageUrl: String,
    val description: String
) : Parcelable
