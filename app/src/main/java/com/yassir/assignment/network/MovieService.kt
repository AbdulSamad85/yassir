package com.yassir.assignment.network

import com.yassir.assignment.common.Constants
import com.yassir.assignment.network.io.model.movie.MoviesResponse
import com.yassir.assignment.network.io.model.movieDetail.MovieDetailResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {

    @GET(Constants.Api.MOVIES_LIST_URL)
    suspend fun getMovies(@Query("page") page: Int): Response<MoviesResponse>

    @GET(Constants.Api.MOVIE_DETAILS_URL)
    suspend fun getMovieDetails(
        @Path("movie_id") movieId: Int
    ): Response<MovieDetailResponse>
}
