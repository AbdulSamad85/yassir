package com.yassir.assignment.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yassir.assignment.R
import com.yassir.assignment.abstraction.BaseViewHolder
import com.yassir.assignment.common.Utils
import com.yassir.assignment.ui.movieList.MoviesViewModel
import com.yassir.assignment.ui_model.UIMovieItem

class MoviesAdapter(
    private val context: Context,
    private val mMovieItems: MutableList<UIMovieItem>?,
    private val moviesViewModel: MoviesViewModel
) :
    RecyclerView.Adapter<BaseViewHolder>() {
    private var isLoaderVisible = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            VIEW_TYPE_NORMAL -> ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.movie_item_view, parent, false)
            )
            else -> ProgressHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.movie_item_progresss_view, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(context, mMovieItems!![position], position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isLoaderVisible) {
            if (position == mMovieItems!!.size - 1) VIEW_TYPE_LOADING else VIEW_TYPE_NORMAL
        } else {
            VIEW_TYPE_NORMAL
        }
    }

    override fun getItemCount(): Int {
        return mMovieItems?.size ?: 0
    }

    fun addItems(movieItems: List<UIMovieItem>) {
        mMovieItems?.addAll(movieItems)
        notifyDataSetChanged()
    }

    fun addLoading() {
        isLoaderVisible = true
        mMovieItems!!.add(UIMovieItem())
        notifyItemInserted(mMovieItems.size - 1)
    }

    fun removeLoading() {
        isLoaderVisible = false
        val position = mMovieItems!!.size - 1
        mMovieItems.removeAt(position)
        notifyItemRemoved(position)
    }

    fun clear() {
        mMovieItems?.clear()
        notifyDataSetChanged()
    }

    fun getItem(position: Int): UIMovieItem {
        return mMovieItems!![position]
    }

    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.title)
        var year: TextView = itemView.findViewById(R.id.year)
        var image: ImageView = itemView.findViewById(R.id.image)

        override fun onBind(context: Context, any: Any, position: Int) {
            super.onBind(context, any, position)
            val item: UIMovieItem = any as UIMovieItem
            title.text = item.title
            year.text = item.year
            Utils.loadThumbnailImage(context, item.imageUrl, image)
            itemView.setOnClickListener {
                moviesViewModel.onMovieSelected(getItem(adapterPosition).movie)
            }
        }
    }

    class ProgressHolder(itemView: View) : BaseViewHolder(itemView) {
        var progressBar: ProgressBar = itemView.findViewById(R.id.progressBar)
    }

    companion object {
        private const val VIEW_TYPE_LOADING = 0
        private const val VIEW_TYPE_NORMAL = 1
    }
}
