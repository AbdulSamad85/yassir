package com.yassir.assignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.yassir.assignment.extensions.toUiModel
import com.yassir.assignment.repository.MovieRepository
import com.yassir.assignment.ui.movieDetails.MovieDetailViewModel
import com.yassir.assignment.ui.movieList.MoviesViewModel
import com.yassir.assignment.ui_model.UIMovieItem
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.*
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class MoviesViewModelTest : BaseTest() {
    private val testDispatcher = TestCoroutineDispatcher()

    @get:Rule
    val instantTaskExecutionRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var movieRepository: MovieRepository
    private lateinit var moviesViewModel: MoviesViewModel
    private lateinit var movieDetailViewModel: MovieDetailViewModel

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
        moviesViewModel = MoviesViewModel(movieRepository)
        movieDetailViewModel = MovieDetailViewModel(movieRepository)
    }

    @Test
    fun `when getMovies() call, should load movies data`() {
        val pageNumber = 1
        val list = getMoviesResponse().results
        val uiMovieList = mutableListOf<UIMovieItem>()

        for (i in list.indices) {
            uiMovieList.add(list[i].toUiModel())
        }
        runBlocking {
            Mockito.`when`(movieRepository.getMovies(pageNumber))
                .thenReturn(Response.success(getMoviesResponse()))
            moviesViewModel.loadMovies()
            val result = moviesViewModel.moviesLiveData.getOrAwaitValue()
            assertEquals(uiMovieList, result)
        }
    }

    @Test
    fun `when getMovieDetails(movieId) call, should load movie detail`() {
        val expectedResponse = getMovieDetailResponse()

        runBlocking {
            Mockito.`when`(movieRepository.getMovieDetails(movieId))
                .thenReturn(Response.success(expectedResponse))
            movieDetailViewModel.loadMovieDetail(movieId)
            val result = movieDetailViewModel.movieDetailLiveData.getOrAwaitValue()
            assertEquals(expectedResponse.toUiModel(), result)
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }
}
