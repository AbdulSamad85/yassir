package com.yassir.assignment.extensions

import com.yassir.assignment.network.io.model.movie.Movie
import com.yassir.assignment.network.io.model.movieDetail.MovieDetailResponse
import com.yassir.assignment.ui_model.UIMovieDetail
import com.yassir.assignment.ui_model.UIMovieItem

/**
 * converts banner to UIBannerItem
 */
fun Movie.toUiModel(): UIMovieItem {
    return UIMovieItem(
        movie = this,
        title = this.title,
        year = this.releaseDate.split("-").first(),
        imageUrl = this.posterPath
    )
}

fun MovieDetailResponse.toUiModel(): UIMovieDetail {
    return UIMovieDetail(
        title = this.title,
        year = this.releaseDate.split("-").first(),
        imageUrl = this.posterPath,
        description = this.overview
    )
}
