package com.yassir.assignment

import com.yassir.assignment.extensions.toUiModel
import org.junit.Assert
import org.junit.Test

class MovieExtTests : BaseTest() {

    @Test
    fun `the Movie UI model should be valid`() {
        val movie = getMoviesResponse().results.first()
        val uiModel = movie.toUiModel()

        Assert.assertTrue(
            uiModel.movie == movie &&
                uiModel.imageUrl == movie.posterPath &&
                uiModel.title == movie.title &&
                uiModel.year == movie.releaseDate.split("-").first()
        )
    }

    @Test
    fun `the MovieDetail UI model should be valid`() {
        val movieDetail = getMovieDetailResponse()
        val uiModel = movieDetail.toUiModel()

        Assert.assertTrue(
            uiModel.imageUrl == movieDetail.posterPath &&
                uiModel.title == movieDetail.title &&
                uiModel.year == movieDetail.releaseDate.split("-").first() &&
                uiModel.description == movieDetail.overview
        )
    }
}
