package com.yassir.assignment.abstraction

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var currentPosition = 0
    open fun onBind(context: Context, any: Any, position: Int) {
        currentPosition = position
    }
}
