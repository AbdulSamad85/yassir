package com.yassir.assignment.ui.movieList

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.yassir.assignment.R
import com.yassir.assignment.abstraction.BaseFragment
import com.yassir.assignment.adapter.MoviesAdapter
import com.yassir.assignment.databinding.FragmentMoviesBinding
import com.yassir.assignment.extensions.observeNonNull
import com.yassir.assignment.ui_model.UIMovieItem
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MoviesFragment : BaseFragment(R.layout.fragment_movies) {

    private val viewModel: MoviesViewModel by activityViewModels()
    private lateinit var binding: FragmentMoviesBinding
    private lateinit var adapter: MoviesAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMoviesBinding.bind(view)
        observeData()
        initRecyclerView()
        viewModel.loadMovies()
    }

    private fun initRecyclerView() {
        binding.moviesRecyclerView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(requireContext())
        binding.moviesRecyclerView.layoutManager = layoutManager
        adapter = MoviesAdapter(requireContext(), mutableListOf(), viewModel)
        binding.moviesRecyclerView.adapter = adapter
        binding.moviesRecyclerView
            .addOnScrollListener(
                viewModel.getPaginationListener(layoutManager)
            )
    }

    private fun observeData() {
        viewModel.run {
            moviesLiveData.observeNonNull(this@MoviesFragment) {
                updateMovies(it)
            }
            progressBarLiveData.observe(viewLifecycleOwner) {
                showProgressBar(it)
            }
            toastLiveData.observe(viewLifecycleOwner) {
                showToast(it)
            }

            showPaginationLoading.observe(viewLifecycleOwner) {
                if (it) {
                    adapter.addLoading()
                } else {
                    adapter.removeLoading()
                }
            }
        }
    }

    private fun updateMovies(movieList: List<UIMovieItem>) {
        adapter.addItems(movieList)
    }

    private fun showProgressBar(visibility: Int) {
        binding.progressBar.visibility = visibility
    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }
}
