package com.yassir.assignment.ui.movieDetails

import android.content.Context
import android.view.View
import android.widget.ImageView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yassir.assignment.common.Utils
import com.yassir.assignment.extensions.toUiModel
import com.yassir.assignment.repository.MovieRepository
import com.yassir.assignment.ui_model.UIMovieDetail
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel
@Inject constructor(
    private val movieRepository: MovieRepository
) : ViewModel() {

    val movieDetailLiveData: LiveData<UIMovieDetail> get() = _movieDetailLiveData
    private val _movieDetailLiveData: MutableLiveData<UIMovieDetail> = MutableLiveData()

    val progressBarLiveData: LiveData<Int> get() = _progressBarLiveData
    private val _progressBarLiveData: MutableLiveData<Int> = MutableLiveData()

    val toastLiveData: LiveData<String> get() = _toastLiveData
    private val _toastLiveData: MutableLiveData<String> = MutableLiveData()

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        throwable.printStackTrace()
        onError("Exception: : ${throwable.localizedMessage}")
    }

    fun loadMovieDetail(movieId: Int) {
        viewModelScope.launch(exceptionHandler) {
            showProgress(true)
            val response = movieRepository.getMovieDetails(movieId)
            showProgress(false)
            if (response.isSuccessful) {
                response.body()?.let {
                    _movieDetailLiveData.postValue(it.toUiModel())
                }
            } else {
                onError("Error : ${response.errorBody()} ")
            }
        }
    }

    private fun showProgress(show: Boolean) {
        val visibility = if (show) View.VISIBLE else View.GONE
        _progressBarLiveData.postValue(visibility)
    }

    private fun onError(message: String) {
        showProgress(false)
        showError(message)
    }

    private fun showError(message: String) {
        _toastLiveData.postValue(message)
    }

    fun loadMovieImage(context: Context, imageView: ImageView, imageUrl: String) {
        Utils.loadThumbnailImage(context, imageUrl, imageView)
    }
}
