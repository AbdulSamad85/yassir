package com.yassir.assignment.repository

import com.yassir.assignment.network.io.model.movie.MoviesResponse
import com.yassir.assignment.network.io.model.movieDetail.MovieDetailResponse
import retrofit2.Response

interface MovieRepository {
    suspend fun getMovies(currentPage: Int): Response<MoviesResponse>
    suspend fun getMovieDetails(movieId: Int): Response<MovieDetailResponse>
}
