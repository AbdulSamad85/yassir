package com.yassir.assignment.ui

import android.os.Bundle
import androidx.activity.viewModels
import com.yassir.assignment.abstraction.BaseActivity
import com.yassir.assignment.databinding.ActivityMainBinding
import com.yassir.assignment.network.io.model.movie.Movie
import com.yassir.assignment.ui.movieDetails.MovieDetailFragment
import com.yassir.assignment.ui.movieList.MoviesFragment
import com.yassir.assignment.ui.movieList.MoviesViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    @Inject
    lateinit var moviesFragmentFactory: MoviesFragmentFactory

    private lateinit var binding: ActivityMainBinding
    val viewModel: MoviesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        observeData()
        loadMoviesFragment()
    }

    private fun observeData() {
        viewModel.apply {
            selectedMovieLiveData.observe(this@MainActivity) {
                loadMovieDetailsFragment(it)
            }
        }
    }

    private fun loadMoviesFragment() {
        supportFragmentManager.fragmentFactory = moviesFragmentFactory
        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .replace(
                binding.container.id, MoviesFragment::class.java, null
            ).commit()
    }

    private fun loadMovieDetailsFragment(movie: Movie) {
        supportFragmentManager.fragmentFactory = moviesFragmentFactory
        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .replace(
                binding.container.id,
                MovieDetailFragment::class.java,
                MovieDetailFragment.createBundle(movie.id)
            )
            .addToBackStack(MoviesFragment::class.simpleName)
            .commit()
    }
}
