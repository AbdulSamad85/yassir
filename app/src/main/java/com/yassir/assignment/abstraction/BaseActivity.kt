package com.yassir.assignment.abstraction

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {
    // keeping for any future usage
}
