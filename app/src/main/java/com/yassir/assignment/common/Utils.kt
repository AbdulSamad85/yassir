package com.yassir.assignment.common

import android.content.Context
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.yassir.assignment.R

object Utils {
    fun loadThumbnailImage(
        context: Context,
        imageUrl: String,
        imageView: ImageView,
    ) {
        val wholeUrl = "${Constants.Api.IMAGE_BASE_URL}w200/$imageUrl"
        loadImage(context, wholeUrl, imageView)
    }

    fun loadBannerImage(
        context: Context,
        imageUrl: String,
        imageView: ImageView,
    ) {
        val wholeUrl = "${Constants.Api.IMAGE_BASE_URL}w400/$imageUrl"
        loadImage(context, wholeUrl, imageView)
    }

    private fun loadImage(
        context: Context,
        imageUrl: String,
        imageView: ImageView,
    ) {
        Picasso.with(context)
            .load(imageUrl)
            .fit()
            .error(R.drawable.placeholder)
            .into(imageView)
    }
}
