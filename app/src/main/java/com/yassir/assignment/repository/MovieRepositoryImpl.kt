package com.yassir.assignment.repository

import com.yassir.assignment.network.MovieService

class MovieRepositoryImpl
constructor(private val movieService: MovieService) : MovieRepository {

    override suspend fun getMovies(currentPage: Int) =
        movieService.getMovies(currentPage)

    override suspend fun getMovieDetails(movieId: Int) =
        movieService.getMovieDetails(movieId)
}
