package com.yassir.assignment

import com.google.gson.Gson
import com.yassir.assignment.network.io.model.movie.MoviesResponse
import com.yassir.assignment.network.io.model.movieDetail.MovieDetailResponse
import java.io.InputStream
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

abstract class BaseTest {
    private val moviesFilePath = "movies_response.json"
    private val movieDetailFilePath = "movie_detail_response.json"
    val movieId = 414906

    fun getMoviesResponse(): MoviesResponse {
        val uri = ClassLoader.getSystemClassLoader().getResource(moviesFilePath).toURI()
        val json = String(
            Files.readAllBytes(Paths.get(uri)),
            Charset.forName(StandardCharsets.UTF_8.name())
        )
        return Gson().fromJson(json, MoviesResponse::class.java)
    }

    fun getMoviesJson(): String {
        val jsonStream: InputStream =
            MoviesResponse::class.java.classLoader!!.getResourceAsStream(moviesFilePath)
        return String(jsonStream.readBytes())
    }

    /**
     * returns response of movie id 414906s
     */
    fun getMovieDetailResponse(): MovieDetailResponse {
        val uri = ClassLoader.getSystemClassLoader().getResource(movieDetailFilePath).toURI()
        val json = String(
            Files.readAllBytes(Paths.get(uri)),
            Charset.forName(StandardCharsets.UTF_8.name())
        )
        return Gson().fromJson(json, MovieDetailResponse::class.java)
    }

    /**
     * returns response of movie id 414906s
     */
    fun getMovieDetailJson(): String {
        val jsonStream: InputStream =
            MovieDetailResponse::class.java.classLoader!!.getResourceAsStream(movieDetailFilePath)
        return String(jsonStream.readBytes())
    }
}
