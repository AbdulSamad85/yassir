package com.yassir.assignment.ui.movieDetails

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.yassir.assignment.R
import com.yassir.assignment.abstraction.BaseFragment
import com.yassir.assignment.common.Utils
import com.yassir.assignment.databinding.FragmentMovieDetailBinding
import com.yassir.assignment.extensions.observeNonNull
import com.yassir.assignment.ui_model.UIMovieDetail
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieDetailFragment :
    BaseFragment(R.layout.fragment_movie_detail) {

    private val viewModel: MovieDetailViewModel by viewModels()
    private lateinit var binding: FragmentMovieDetailBinding

    companion object {
        private const val KEY_MOVIE_ID = "KEY_MOVIE_ID"
        fun createBundle(movieId: Int): Bundle {
            return Bundle().apply {
                putInt(KEY_MOVIE_ID, movieId)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMovieDetailBinding.bind(view)
        observeData()
        arguments?.let {
            handleBundle(it)
        }
    }

    private fun observeData() {
        viewModel.run {
            movieDetailLiveData.observeNonNull(this@MovieDetailFragment) {
                updateUi(it)
            }
            progressBarLiveData.observe(viewLifecycleOwner) {
                showProgressBar(it)
            }
            toastLiveData.observe(viewLifecycleOwner) {
                showToast(it)
            }
        }
    }

    private fun updateUi(uiMovieDetail: UIMovieDetail) {
        binding.title.text = uiMovieDetail.title
        binding.year.text = uiMovieDetail.year
        binding.description.text = uiMovieDetail.description
        Utils.loadBannerImage(requireContext(), uiMovieDetail.imageUrl, binding.image)
    }

    private fun showProgressBar(visibility: Int) {
        binding.progressBar.visibility = visibility
    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    private fun handleBundle(bundle: Bundle) {
        val movieId = bundle.getInt(KEY_MOVIE_ID)
        viewModel.loadMovieDetail(movieId)
    }
}
