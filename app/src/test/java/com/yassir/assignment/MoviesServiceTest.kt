package com.yassir.assignment

import com.google.gson.Gson
import com.yassir.assignment.common.Constants.Api.Companion.MOVIES_LIST_URL
import com.yassir.assignment.common.Constants.Api.Companion.MOVIE_DETAILS_URL
import com.yassir.assignment.network.MovieService
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class MoviesServiceTest : BaseTest() {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var movieService: MovieService

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        mockWebServer = MockWebServer()
        movieService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .build().create(MovieService::class.java)
    }

    @Test
    fun `get movies api test`() {
        val pageNumber = 1
        runBlocking {
            val mockResponse = MockResponse()
            mockWebServer.enqueue(mockResponse.setBody(getMoviesJson()))
            val response = movieService.getMovies(pageNumber)
            val request = mockWebServer.takeRequest()
            val expectedPath = "/$MOVIES_LIST_URL&page=$pageNumber"
            assertEquals(expectedPath, request.path)
            assertTrue(response.body() == getMoviesResponse())
        }
    }

    @Test
    fun `get movie detail api test`() {
        runBlocking {
            val mockResponse = MockResponse()
            mockWebServer.enqueue(mockResponse.setBody(getMovieDetailJson()))
            val response = movieService.getMovieDetails(movieId)
            val request = mockWebServer.takeRequest()
            val expectedPath = "/" + MOVIE_DETAILS_URL
                .replace("{movie_id}", movieId.toString())
            assertEquals(expectedPath, request.path)
            assertTrue(response.body() == getMovieDetailResponse())
        }
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }
}
