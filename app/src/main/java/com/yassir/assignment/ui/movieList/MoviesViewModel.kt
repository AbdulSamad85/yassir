package com.yassir.assignment.ui.movieList

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.yassir.assignment.extensions.toUiModel
import com.yassir.assignment.listener.PaginationListener
import com.yassir.assignment.listener.PaginationListener.Companion.PAGE_START
import com.yassir.assignment.network.io.model.movie.Movie
import com.yassir.assignment.network.io.model.movie.MoviesResponse
import com.yassir.assignment.repository.MovieRepository
import com.yassir.assignment.ui_model.UIMovieItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import okhttp3.internal.toImmutableList
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel
@Inject constructor(
    private val movieRepository: MovieRepository
) : ViewModel() {

    private var currentPage: Int = PAGE_START
    private var isLastPage = false
    private var totalPage = PAGE_START
    private var isLoading = false
    var itemCount = 0

    fun getPaginationListener(linearLayoutManager: LinearLayoutManager): PaginationListener {
        return object : PaginationListener(linearLayoutManager) {
            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                loadMovies()
            }

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }
        }
    }

    val moviesLiveData: LiveData<List<UIMovieItem>> get() = _moviesLiveData
    private val _moviesLiveData: MutableLiveData<List<UIMovieItem>> = MutableLiveData()

    val selectedMovieLiveData: LiveData<Movie> get() = _selectedMovieLiveData
    private val _selectedMovieLiveData: MutableLiveData<Movie> = MutableLiveData()

    val progressBarLiveData: LiveData<Int> get() = _progressBarLiveData
    private val _progressBarLiveData: MutableLiveData<Int> = MutableLiveData()

    val showPaginationLoading: LiveData<Boolean> get() = _showPaginationLoading
    private val _showPaginationLoading: MutableLiveData<Boolean> = MutableLiveData()

    val toastLiveData: LiveData<String> get() = _toastLiveData
    private val _toastLiveData: MutableLiveData<String> = MutableLiveData()

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        throwable.printStackTrace()
        onError("Exception: : ${throwable.localizedMessage}")
    }

    fun loadMovies() {
        viewModelScope.launch(exceptionHandler) {
            showProgress(true)
            val response = movieRepository.getMovies(currentPage)
            showProgress(false)
            if (response.isSuccessful) {
                response.body()?.let { it ->
                    onMoviesSuccessResponse(it)
                }
            } else {
                onError("Error : ${response.errorBody()} ")
            }
        }
    }

    private fun onMoviesSuccessResponse(moviesResponse: MoviesResponse) {
        totalPage = moviesResponse.totalPages
        val uiModelList = mutableListOf<UIMovieItem>()
        moviesResponse.results.forEach { movie ->
            itemCount++
            uiModelList.add(movie.toUiModel())
        }
        if (currentPage != PAGE_START) {
            _showPaginationLoading.value = false
        }
        _moviesLiveData.value = uiModelList.toImmutableList()
        if (currentPage < totalPage) {
            _showPaginationLoading.value = true
        } else {
            isLastPage = true
        }
        isLoading = false
    }

    private fun showProgress(show: Boolean) {
        if (currentPage == 1) {
            val visibility = if (show) View.VISIBLE else View.GONE
            _progressBarLiveData.postValue(visibility)
        }
    }

    private fun onError(message: String) {
        showProgress(false)
        showError(message)
    }

    private fun showError(message: String) {
        _toastLiveData.postValue(message)
    }

    fun onMovieSelected(movie: Movie) {
        _selectedMovieLiveData.value = movie
    }
}
