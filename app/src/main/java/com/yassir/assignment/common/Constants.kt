package com.yassir.assignment.common

import com.yassir.assignment.common.Constants.Api.Header.Companion.VALUE_API_KEY

class Constants {
    sealed class Api {
        class Header {
            companion object {
                const val KEY_CONTENT_TYPE = "Content-Type"
                const val KEY_CONTENT_TYPE_VALUE = "application/json;charset=utf-8"
                const val VALUE_API_KEY =
                    "c9856d0cb57c3f14bf75bdc6c063b8f3"
            }
        }
        companion object {
            const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/"
            const val BASE_URL = "https://api.themoviedb.org/3/"
            const val MOVIES_LIST_URL = "discover/movie?api_key=$VALUE_API_KEY"
            const val MOVIE_DETAILS_URL = "movie/{movie_id}?api_key=$VALUE_API_KEY"
        }
    }
}
