package com.yassir.assignment.network.io.model.movie

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MoviesResponse(
    @SerializedName("page") val page: Int?,
    @SerializedName("results") val results: ArrayList<Movie>,
    @SerializedName("total_pages") val totalPages: Int = 0,
    @SerializedName("total_results") val totalResults: Int?
) : Parcelable
